@area
Feature: User starts the app after installation

  @first
  Scenario: User start to work with the app
    Given I have no internet (wi-fi is not available)
    When I start the app
    Then I see the text about "Спасибо, что установили Kaspersky Secure Connection!"
    And the text under it "Следуйте инструкциям на экране, чтобы настроить приложение."
    And I see the green button with text "ДАЛЕЕ"
    When I click this button with text "ДАЛЕЕ"
    Then I see the page with permissions (header contains "Нужны разрешения")

  Scenario: User sees the page with permissions
    When I see the page with permissions (header contains "Нужны разрешения")
    Then I see the text under it "В следующих окнах разрешите приложению выполнять указанные действия."
    And the explain "Доступ к телефону нужен для запуска приложения."
    And the button for agreement "ОК"
    When I click the button for agreement
    Then the dialog window opens
    And it contains the question "Разрешить приложению Kaspersky Secure Conne… осуществление телефонных звонков и управление ими?"
    And two button "ОТКЛОНИТЬ"
    And the button "РАЗРЕШИТЬ"
    When I click "ОТКЛОНИТЬ"
    And I click "ОК"
    Then the dialog window appears again
    When the

  @noint
  Scenario: User checks the connection with Internet
    Given I have no internet (wifi is not available)
    When I check the picture on screen
    Then it will be grey
    When I check the text under the picture
    Then I see the text "Нет подключения к интернету"